import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate';
Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [createPersistedState()],
  state: {
    count:0,
    accessToken:''
  },
  getters: {
    isLoggedIn: state => {
      return state.accessToken;
    }
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.accessToken = token;
    },

  },
  actions: {
    login: ({ commit }, { token }) => {
      commit('SET_TOKEN', token);
    },
    logout: ({ commit }) => {
      commit('SET_TOKEN', '');
    }
  },
  modules: {
  }
})
