import axios from 'axios';
export default {
    async getUsers(){
        try {
            const data = await axios.get('api/users')
            return data.data
        }catch (e) {
            return false
        }
    },
    async getPeople(cobasa)
    {
        const data = await axios.get('api/people?cobasa='+cobasa)
        return data.data
    },
    async store(data)
    {
        try {
            const response = await axios.post('api/users',data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async storePerson(data)
    {
        try {
            const response = await axios.post('api/people',data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async edit(data)
    {
        try{
            const response = await axios.put('api/users/'+data.id,data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async editPeople(data)
    {
        try{
            const response = await axios.put('api/people/'+data.id,data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async delete(data)
    {
        try{
            const response = await axios.delete('api/users/'+data.id)
            return response.data
        }catch (e) {
            return false
        }
    }
    , async deletePeople(data)
    {
        try{
            const response = await axios.delete('api/people/'+data.id)
            return response.data
        }catch (e) {
            return false
        }
    },
    async getCurrentUser(){
        try {
            const data = await axios.get('api/me')
            return data
            //return response.data.data
            //return true
        }catch (e) {
            return e.response;

        }
    }

}
