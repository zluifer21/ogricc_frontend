import axios from 'axios';
export default {
    async create()
    {
        const data= await axios.get('api/recordsites-create')
        return data.data.data;
    },
    async getItems()
    {
        const data= await axios.get('api/recordsites')
        return data.data;
    },
    async getRecord(id)
    {
        const data= await axios.get('api/recordsites/'+id)
        return data.data;
    },
    async store(data,affectation_type_id){
        try {
            let bodyFormData = new FormData();
            data.images.forEach((image,i)=>{
                bodyFormData.append(`images[${i}]`,image);
            })
            data.raitings.forEach((raiting,i)=>{
                bodyFormData.append(`ratings[${i}]`,raiting);
            })
            let url='';
            if(data.id){
                bodyFormData.append('_method', 'put')
                url= 'api/recordsites/'+data.id
            }
            else
            {
                url= 'api/recordsites'
            }
            affectation_type_id.forEach((afecctation,i)=>{
                bodyFormData.append(`affectation_type[${i}]`,parseInt(afecctation))
            })
            bodyFormData.append('latitud',data.latitude);
            bodyFormData.append('longitud',data.longitude);
            bodyFormData.append('neighborhood_id',data.neighborhood_id);
            bodyFormData.append('characteristics_place_id',data.characteristics_place_id);
            bodyFormData.append('alert_level_id',data.alert_level_id);
            bodyFormData.append('duration',data.duration);
            bodyFormData.append('n_families',data.n_families);
            bodyFormData.append('n_homes',data.n_homes);
            bodyFormData.append('n_schools',data.n_schools);
            bodyFormData.append('n_healthcares',data.n_healthcares);
            bodyFormData.append('n_person_affecteds',data.n_person_affecteds);
            bodyFormData.append('risks_zone_id',data.risks_zone_id);
            bodyFormData.append('floor_type_id',data.floor_type_id);
            bodyFormData.append('affectation_type_id',data.affectation_type_id);
            bodyFormData.append('n_person_wounds',data.n_person_wounds);
            bodyFormData.append('n_person_deads',data.n_person_deads);
            bodyFormData.append('observations',data.observations);
            bodyFormData.append('date',data.date);
            bodyFormData.append('time',data.time);
            const response = await axios.post(url,bodyFormData,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            return response;
        }catch (e) {
            return e;
        }

    }
    , async delete(data)
    {
        try{
            const response = await axios.delete('api/recordsites/'+data.id)
            return response.data
        }catch (e) {
            return false
        }
    },
    async clone(id){
        const data = await axios.get('api/recor_sites_clone?id='+id)
        return data.data;
    },
}
