import axios from 'axios';
export default {
    async edit(data)
    {
        try{
            const response = await axios.put('api/bitacoras/'+data.id,data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async store(data)
    {
        try {
            const response = await axios.post('api/bitacoras',data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async getItems(){
        try{
            const  data = await axios.get('api/bitacoras')
            return data.data;
        }catch (e) {
            return e
        }
    }
    , async delete(data)
    {
        try{
            const response = await axios.delete('api/bitacoras/'+data.id)
            return response.data
        }catch (e) {
            return false
        }
    }
}
