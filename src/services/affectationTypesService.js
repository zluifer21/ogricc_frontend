import axios from 'axios';
export default {
    async getItems(){
        try{
            const  data = await axios.get('api/affectation_types')
            return data.data.data;
        }catch (e) {
            return e
        }

    }
}
