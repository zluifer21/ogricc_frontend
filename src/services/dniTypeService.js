import axios from 'axios';
export default {
    async getDniType(){
        try {
            const data = await axios.get('api/identification_types')
            return data.data
        }catch (e) {
            return false
        }
    }
}
