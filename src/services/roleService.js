import axios from 'axios';
export default {
    async getRoles(){
        try {
            const data = await axios.get('api/roles')
            return data.data
        }catch (e) {
            return false
        }
    }
}
