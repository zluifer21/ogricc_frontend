import axios from 'axios';
export default {
    async getGroups(){
        try {
            const data = await axios.get('api/areas')
            return data.data
        }catch (e) {
            return false
        }
    },
    async store(data)
    {
        try {
            const response = await axios.post('api/areas',data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async edit(data)
    {
        try{
            const response = await axios.put('api/areas/'+data.id,data)
            return response.data
        }catch (e) {
            return false
        }
    },
    async delete(data)
    {
        try{
            const response = await axios.delete('api/areas/'+data.id)
            return response.data
        }catch (e) {
            return false
        }
    },
    async getUsersArea()
    {
        try {
            const data = await axios.get('api/areas-user')
            return data.data
        }catch (e) {
            return false
        }
    }

}
