import axios from 'axios';
export default {
    async getItems()
    {
        const data= await axios.get('api/recor_homes')
        return data.data;
    },
    async getRecord(id)
    {
        const data= await axios.get('api/recor_homes/'+id)
        return data.data;
    },
    async create() {
        const data = await axios.get('api/recordhome-create')
        return data.data.data;
    },
    async clone(id){
        const data = await axios.get('api/recor_homes_clone?id='+id)
        return data.data;
    },
    async store(data,services,affectation_type_id,help_id,id){
        try {
            let bodyFormData = new FormData();
            if(data.images){
                data.images.forEach((image,i)=>{
                    bodyFormData.append(`images[${i}]`,image);
                })
            }
            let url='';
            if(id){
                bodyFormData.append('_method', 'put')
                 url= 'api/recor_homes/'+id
            }
            else
                {
                   url= 'api/recor_homes'
                }
            services.forEach((service,i)=>{
                bodyFormData.append(`services[${i}]`,parseInt(service))
            })
            help_id.forEach((help,i)=>{
                bodyFormData.append(`helps[${i}]`,parseInt(help))
            })
            affectation_type_id.forEach((afecctation,i)=>{
                bodyFormData.append(`affectation_type[${i}]`,parseInt(afecctation))
            })
            bodyFormData.append('data', JSON.stringify(data))
            const response = await axios.post(url,bodyFormData,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            return response.data;
        }catch (e) {
            console.log(e)
            return false;
        }
    },
    async export(){
        const data= await axios.get('api/recor_homes_export');
        return data.data;
    }

}
