import axios from 'axios';
export default {
    async login(email,password) {
        try {
            const res= await  axios.post( 'oauth/token', {
                grant_type: 'password',
                client_id: 2,
                client_secret: process.env.VUE_APP_CLIENT_SECRET,
                username: email,
                password,
            })
            return res.data
        }catch (e) {
            return false;
        }
    }
};
