import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import vueaxios from "@/plugins/vueaxios";
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import LaravelPermissions from "@/plugins/LaravelPermissions";
import "@/plugins/vuetify-money.js";
import Vuetoast from "@/plugins/toast";
//import VueCharts from "@/plugins/VueCharts"
import Vuelidate from "@/plugins/vuelidate"
delete L.Icon.Default.prototype._getIconUrl




L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

import store from './store'
Vue.config.productionTip = false
new Vue({
  vuetify,
  vueaxios,
  router,
  Vuetoast,
  Vuelidate,
  LaravelPermissions,
  store,
  //VueCharts,
  render: h => h(App)
}).$mount('#app')
