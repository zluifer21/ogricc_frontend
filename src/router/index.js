import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/Login";


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    component: () => import('@/views/dashboard/Index')
  },
  {
    path: '/sitio/',
    name: 'sitio',
    component: () => import('@/views/RecordSite/Index')

  },
 {
    path: '/sitio/create',
    name: 'sitio_create',
    component: () => import('@/views/RecordSite/Create')
  },
  {
    path: '/visita/',
    name: 'visita',
    component: () => import('@/views/Visitas/Index')

  },
  {
      path: '/visita/edit/:id',
      name: 'visita_edit',
      component: () => import('@/views/Visitas/Create')

  },
      {
          path: '/sitio/edit/:id',
          name: 'sitio_edit',
          component: () => import('@/views/RecordSite/Create')

      },
  {
    path: '/visita/create',
    name: 'visita_create',
    component: () => import('@/views/Visitas/Create')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: Login

  },
  {
    path: '/grupos',
    name: 'groups',
    component: ()=>import('../views/Groups/Index')

  },
    {
      path: '/usuarios',
      name: 'users',
      component: ()=>import('../views/Users/Index')

    },
    {
      path: '/cobasas',
      name: 'cobasa',
      component: ()=>import('../views/Cobasa/Index')

    },
    {
      path: '/bitacoras',
      name: 'bitacoras',
      component: ()=>import('../views/Bitacoras/Index')

    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
